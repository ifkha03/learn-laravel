<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
membuat route

Route::<http-method>('<url>', <function callback>)
*/

Route::get('/', 'MainController@index');
Route::get('/identitas', 'IdentitasController@index');
//Route::post('/form-action', 'IdentitasController@store');
//Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@index');
Route::post('/reg', 'AuthController@enter');
Route::get('/data-tables', 'IndexController@datatab');

Route::get('/master', function(){
    return view('layouts.master');
});

//maaf malah jadi error terus kalau pakai cast_id
Route::group([
    'prefix' => 'cast',
    'as' => 'cast.'
], function() {
    Route::get('/', 'CastController@index')->name('index');
    Route::get('/create', 'CastController@create')->name('create');
    Route::post('/', 'CastController@store')->name('store');
    Route::get('/{cast}', 'CastController@show')->name('show');
    Route::get('/{cast}/edit', 'CastController@edit')->name('edit');
    Route::put('/{cast}', 'CastController@update')->name('update');
    Route::delete('/{cast}', 'CastController@destroy')->name('destroy');
});




Route::group([
    'prefix' => 'post',
    'as' => 'post.'
], function() {
    Route::get('/', 'PostController@index')->name('index');
    Route::get('/create', 'PostController@create')->name('create');
    Route::post('/', 'PostController@store')->name('store');
    Route::get('/{post}', 'PostController@show')->name('show');
    Route::get('/{post}/edit', 'PostController@edit')->name('edit');
    Route::put('/{post}', 'PostController@update')->name('update');
    Route::delete('/{post}', 'PostController@destroy')->name('destroy');
});

Route::get('/game', 'GameController@index')->name('game.index');
Route::get('/game/create', 'GameController@create')->name('game.create');
Route::post('/game', 'GameController@store')->name('game.store');
Route::get('/game/{game}', 'GameController@show')->name('game.show');
Route::get('/game/{game}/edit', 'GameController@edit')->name('game.edit');
Route::put('/game/{game}', 'GameController@update')->name('game.update');
Route::delete('/game/{game}', 'GameController@destroy')->name('game.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

