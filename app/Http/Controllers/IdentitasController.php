<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IdentitasController extends Controller
{
    //
    public function index()
    {
        return view('identitas');
    }

    public function store(Request $data){
        //melihat bbrapa data dari get data
        //dd($data);
        $nama = $data->nama;
        $hobi = $data->hobi;
        return view('sambutan', compact('nama', 'hobi'));
    }
}
