<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;


class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    {
        //
        $cast = Cast::all();
        return view('cast.index', compact('cast'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=> 'required',
            'umur'=> 'required',
            'bio'=> 'required'
        ]);

        Cast::create([
            'name' => $request->name,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cast $cast)
    {
        //
        return view('cast.show', compact('cast'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cast $cast)
    {
        //
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Cast $cast,Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

      
        $cast->name = $request->name;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cast $cast)
    {
        //
        $cast->delete();
        return redirect()->route('cast.index');
    }
}
