<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index()
    {
        return view('register');
    }

    public function enter(Request $data)
    {
        $fname = $data->fname;
        $lname = $data->lname;
        return view('welcome', compact('fname', 'lname'));
    }
}
