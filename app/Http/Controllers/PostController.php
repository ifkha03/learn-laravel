<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $post = Post::all();
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     * untuk membuat data
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');

    }

    /**
     * Store a newly created resource in storage.
     * menyimpan data
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title'=> 'required',
            'body'=> 'required'
        ]);

        Post::create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        
        return view('post.show', compact('post'));

    }

    /**
     * Show the form for editing the specified resource.
     * mengubah data
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
      
        return view('post.edit', compact('post'));
    }

    public function update(Post $post, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:posts',
            'body' => 'required',
        ]);

      
        $post->title = $request->title;
        $post->body = $request->body;
        $post->update();
        return redirect('/post');
        
    }

    /**
     * Remove the specified resource from storage.
     * menghapus data
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
       
        $post->delete();
        return redirect()->route('post.index');
    }
}
