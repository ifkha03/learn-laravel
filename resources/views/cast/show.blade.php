@extends('layouts.master')
@section('title', 'Detail Cast')

@section('content')
    <h2>Show Cast {{$cast->id}}</h2>
    <h4>{{$cast->name}}</h4>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection